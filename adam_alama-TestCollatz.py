#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):  # Two positives
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):  # Both Positive
        s = "5 25\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  5)
        self.assertEqual(j, 25)

    def test_read_3(self):  # i negative, j positive
        s = "-9 14\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  -9)
        self.assertEqual(j, 14)

    def test_read_4(self):  # i = 0, j = 0
        s = "0 0\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  0)
        self.assertEqual(j, 0)

    def test_read_5(self):  # i negative, j negative
        s = "-7 -502\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  -7)
        self.assertEqual(j, -502)

    def test_read_6(self):  # i postitive, j negative
        s = "1 -10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, -10)

    def test_read_7(self):  # i positive, j = 0
        s = "1 0\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 0)

    def test_read_8(self):  # i positive, j = 0
        s = "67 54\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  67)
        self.assertEqual(j, 54)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(5, 6)
        self.assertEqual(v, 9)

    def test_eval_6(self):
        v = collatz_eval(-3, 200)
        self.assertEqual(v, 0)

    def test_eval_7(self):
        v = collatz_eval(10000, 10001)
        self.assertEqual(v, 180)

    def test_eval_8(self):
        v = collatz_eval(45, -65)
        self.assertEqual(v, 0)

    def test_eval_9(self):
        v = collatz_eval(67, 54)
        self.assertEqual(v, 113)

    def test_eval_10(self):
        v = collatz_eval(1, 4)
        self.assertEqual(v, 8)

    def test_eval_11(self):
        v = collatz_eval(1, 6)
        self.assertEqual(v, 9)

    def test_eval_12(self):
        v = collatz_eval(5, 8)
        self.assertEqual(v, 17)

    def test_eval_13(self):
        v = collatz_eval(500, 2)
        self.assertEqual(v, 144)

    def test_eval_14(self):
        v = collatz_eval(1, 2)
        self.assertEqual(v, 2)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("5 6\n-3 200\n10000 10001\n45 -65\n67 54\n1 4\n1 6\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5 6 9\n-3 200 0\n10000 10001 180\n45 -65 0\n67 54 113\n1 4 8\n1 6 9\n")

    def test_solve_3(self):
        r = StringIO("5 8\n500 2\n1 2\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5 8 17\n500 2 144\n1 2 2\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
